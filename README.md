# Funny Exchange Rate App

Funny Exchange Rate App  
giphy.com   
openexchangerates.org  
Feign  
Graddle  
Tests  

# Как запускать приложение

1. Запустите Main метод в классе ExchangeRateApplication
2. В браузере запустите http://localhost:9090/rate/funnyRate/{charCode}
Вместо {charCode} необходимо вставить код валюты, например, RUB
3. Чтобы увидеть все доступные {charCode}, откройте в браузере
   http://localhost:9090/rate/codes
4. Дополнительные страницы:  
4.1. http://localhost:9090/rate/latest/{charCode}  
Вместо {charCode} необходимо вставить код валюты, например, RUB
На этой странице можно посмотреть текущий курс валюты 
{charCode} относительно USD  
4.2. http://localhost:9090/rate/history/{charCode}  
   Вместо {charCode} необходимо вставить код валюты, например, RUB
   На этой странице можно посмотреть курс валюты за вчера
   {charCode} относительно USD  
4.3.  http://localhost:9090/giphy/random/broke
На этой странице можно посмотреть параметры рандомной картинки
из раздела broke  
4.4. http://localhost:9090/giphy/random/rich  
   На этой странице можно посмотреть параметры рандомной картинки
   из раздела rich 
5. Параметры приложения (application.properties):  
5.1. openexchangerates.appId - ключ приложения в сервисе openexchangerates.org    
5.2. giphy.appKey - ключ приложения в сервисе giphy.com  
5.3. server.port - порт  
5.4. giphy.rating - рейтинг в сервисе giphy.com  
5.5. giphy.tag1, giphy.tag2 - теги в сервисе giphy.com  
5.6. openexchangerates.uri, giphy.uri - uri в сервисах openexchangerates.org, giphy.com 
