package ru.slovetskikh.exchangerateapp.feign.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import ru.slovetskikh.exchangerateapp.feign.client.IGiphyClient;
import ru.slovetskikh.exchangerateapp.feign.rest.response.Giphy;
import ru.slovetskikh.exchangerateapp.feign.service.ExchangeRateService;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

@SpringBootTest
@RunWith(SpringRunner.class)
class GiphyControllerTest {

    @Autowired
    private IGiphyClient iGiphyClient;

    @Autowired
    private ExchangeRateService exchangeRateService;

    @BeforeAll
    public static void setup() {
        Calendar calendarNow = Calendar.getInstance();
        String formattedData = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateToday", formattedData);

        calendarNow.add(Calendar.DAY_OF_YEAR, -1);
        String formattedData2 = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateYestarday", formattedData2);
    }

    @Test
    void getgiphyRich() {
        Map<Object, Object> giphyClient = Objects.requireNonNull(iGiphyClient.getGiphyRichy().getBody());
        String gifId = exchangeRateService.getGiphyId(giphyClient);
        Map<Object, Object> expectedGiphyClient = Objects.requireNonNull(iGiphyClient.getMockGiphyRichById(gifId).getBody());
        Giphy giphy = new Giphy (
                exchangeRateService.getGiphyType(giphyClient),
                exchangeRateService.getGiphyId(giphyClient),
                exchangeRateService.getGiphyUrl(giphyClient),
                exchangeRateService.getGiphyTitle(giphyClient),
                exchangeRateService.getGiphyGif(giphyClient)
        );

        Giphy expectedGiphy = new Giphy (
                exchangeRateService.getGiphyType(expectedGiphyClient),
                exchangeRateService.getGiphyId(expectedGiphyClient),
                exchangeRateService.getGiphyUrl(expectedGiphyClient),
                exchangeRateService.getGiphyTitle(expectedGiphyClient),
                exchangeRateService.getGiphyGif(expectedGiphyClient)
        );
        Assertions.assertEquals(ResponseEntity.ok(expectedGiphy), ResponseEntity.ok(giphy));
    }

    @Test
    void getGiphyBroke() {
        Map<Object, Object> giphyClient = Objects.requireNonNull(iGiphyClient.getGiphyBroke().getBody());
        String gifId = exchangeRateService.getGiphyId(giphyClient);
        Map<Object, Object> expectedGiphyClient = Objects.requireNonNull(iGiphyClient.getMockGiphyRichById(gifId).getBody());
        Giphy giphy = new Giphy (
                exchangeRateService.getGiphyType(giphyClient),
                exchangeRateService.getGiphyId(giphyClient),
                exchangeRateService.getGiphyUrl(giphyClient),
                exchangeRateService.getGiphyTitle(giphyClient),
                exchangeRateService.getGiphyGif(giphyClient)
        );

        Giphy expectedGiphy = new Giphy (
                exchangeRateService.getGiphyType(expectedGiphyClient),
                exchangeRateService.getGiphyId(expectedGiphyClient),
                exchangeRateService.getGiphyUrl(expectedGiphyClient),
                exchangeRateService.getGiphyTitle(expectedGiphyClient),
                exchangeRateService.getGiphyGif(expectedGiphyClient)
        );
        Assertions.assertEquals(ResponseEntity.ok(expectedGiphy), ResponseEntity.ok(giphy));
    }
}
