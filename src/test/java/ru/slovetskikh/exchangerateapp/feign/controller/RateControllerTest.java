package ru.slovetskikh.exchangerateapp.feign.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import ru.slovetskikh.exchangerateapp.feign.client.IRateClient;
import ru.slovetskikh.exchangerateapp.feign.rest.response.Exchange;
import ru.slovetskikh.exchangerateapp.feign.service.ExchangeRateService;

import java.text.SimpleDateFormat;
import java.util.*;

@SpringBootTest
@RunWith(SpringRunner.class)
class RateControllerTest {

    @Value("${openexchangerates.appId}")
    private String openexchangeratesAppId;

    @Value("${openexchangerates.dateToday}")
    private String dateToday;

    @Value("${openexchangerates.dateYestarday}")
    private String dateYesterday;

    @Autowired
    private IRateClient iRateClient;

    @Autowired
    private ExchangeRateService exchangeRateService;

    @BeforeAll
    public static void setup() {
        Calendar calendarNow = Calendar.getInstance();
        String formattedData = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateToday", formattedData);

        calendarNow.add(Calendar.DAY_OF_YEAR, -1);
        String formattedData2 = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateYestarday", formattedData2);
    }

    @Test
    void getRateHistory() {
        String openexchangeratesRate = "RUB";
        Map<Object, Object> expectedRateClient = Objects.requireNonNull(iRateClient.getRateHistory(openexchangeratesAppId, dateYesterday, openexchangeratesRate).getBody());
        Optional<Double> expectedCurrentRateResponse = Optional.of(exchangeRateService.getRate(expectedRateClient, openexchangeratesRate));
        String expectedDisclaimer = "Usage subject to terms: https://openexchangerates.org/terms";
        String expectedLicense ="https://openexchangerates.org/license";
        String expectedBase = "USD";

        Exchange expectedExchange = new Exchange(
                expectedDisclaimer,
                expectedLicense,
                expectedBase,
                expectedCurrentRateResponse.get(),
                openexchangeratesRate,
                dateYesterday
        );

        Map<Object, Object> rateClient = Objects.requireNonNull(iRateClient.getRateHistory(openexchangeratesAppId, dateYesterday, openexchangeratesRate).getBody());
        Optional<Double> currentRubRateResponse = Optional.of(exchangeRateService.getRate(rateClient, openexchangeratesRate));
        String disclaimer = exchangeRateService.getDisclaimer(rateClient);
        String license = exchangeRateService.getLicense(rateClient);
        String base = exchangeRateService.getBase(rateClient);
        Exchange exchange = new Exchange(
                disclaimer,
                license,
                base,
                currentRubRateResponse.get(),
                openexchangeratesRate,
                dateYesterday
        );
        Assertions.assertEquals(ResponseEntity.ok(expectedExchange), ResponseEntity.ok(exchange));
    }

    @Test
    void getRateLatest() {
        String openexchangeratesRate = "RUB";
        Map<Object, Object> expectedRateClient = Objects.requireNonNull(iRateClient.getRateHistoryToday(openexchangeratesAppId, dateToday, openexchangeratesRate).getBody());
        Optional<Double> expectedCurrentRateResponse = Optional.of(exchangeRateService.getRate(expectedRateClient, openexchangeratesRate));
        String expectedDisclaimer = "Usage subject to terms: https://openexchangerates.org/terms";
        String expectedLicense ="https://openexchangerates.org/license";
        String expectedBase = "USD";

        Exchange expectedExchange = new Exchange(
                expectedDisclaimer,
                expectedLicense,
                expectedBase,
                expectedCurrentRateResponse.get(),
                openexchangeratesRate,
                dateToday
        );

        Map<Object, Object> rateClient = Objects.requireNonNull(iRateClient.getRateLatest(openexchangeratesAppId, openexchangeratesRate).getBody());
        Optional<Double> currentRubRateResponse = Optional.of(exchangeRateService.getRate(rateClient, openexchangeratesRate));
        String disclaimer = exchangeRateService.getDisclaimer(rateClient);
        String license = exchangeRateService.getLicense(rateClient);
        String base = exchangeRateService.getBase(rateClient);
        Exchange exchange = new Exchange(
                disclaimer,
                license,
                base,
                currentRubRateResponse.get(),
                openexchangeratesRate,
                dateToday
        );
        Assertions.assertEquals(ResponseEntity.ok(expectedExchange), ResponseEntity.ok(exchange));
    }

    @Test
    void getCharCodes() {
        List<String> charCodes = new LinkedList<>();
        charCodes.add("AED");
        charCodes.add("AFN");
        charCodes.add("ALL");
        charCodes.add("AMD");
        charCodes.add("ANG");
        charCodes.add("AOA");
        charCodes.add("ARS");
        charCodes.add("AUD");
        charCodes.add("AWG");
        charCodes.add("AZN");
        charCodes.add("BAM");
        charCodes.add("BBD");
        charCodes.add("BDT");
        charCodes.add("BGN");
        charCodes.add("BHD");
        charCodes.add("BIF");
        charCodes.add("BMD");
        charCodes.add("BND");
        charCodes.add("BOB");
        charCodes.add("BRL");
        charCodes.add("BSD");
        charCodes.add("BTC");
        charCodes.add("BTN");
        charCodes.add("BWP");
        charCodes.add("BYN");
        charCodes.add("BZD");
        charCodes.add("CAD");
        charCodes.add("CDF");
        charCodes.add("CHF");
        charCodes.add("CLF");
        charCodes.add("CLP");
        charCodes.add("CNH");
        charCodes.add("CNY");
        charCodes.add("COP");
        charCodes.add("CRC");
        charCodes.add("CUC");
        charCodes.add("CUP");
        charCodes.add("CVE");
        charCodes.add("CZK");
        charCodes.add("DJF");
        charCodes.add("DKK");
        charCodes.add("DOP");
        charCodes.add("DZD");
        charCodes.add("EGP");
        charCodes.add("ERN");
        charCodes.add("ETB");
        charCodes.add("EUR");
        charCodes.add("FJD");
        charCodes.add("FKP");
        charCodes.add("GBP");
        charCodes.add("GEL");
        charCodes.add("GGP");
        charCodes.add("GHS");
        charCodes.add("GIP");
        charCodes.add("GMD");
        charCodes.add("GNF");
        charCodes.add("GTQ");
        charCodes.add("GYD");
        charCodes.add("HKD");
        charCodes.add("HNL");
        charCodes.add("HRK");
        charCodes.add("HTG");
        charCodes.add("HUF");
        charCodes.add("IDR");
        charCodes.add("ILS");
        charCodes.add("IMP");
        charCodes.add("INR");
        charCodes.add("IQD");
        charCodes.add("IRR");
        charCodes.add("ISK");
        charCodes.add("JEP");
        charCodes.add("JMD");
        charCodes.add("JOD");
        charCodes.add("JPY");
        charCodes.add("KES");
        charCodes.add("KGS");
        charCodes.add("KHR");
        charCodes.add("KMF");
        charCodes.add("KPW");
        charCodes.add("KRW");
        charCodes.add("KWD");
        charCodes.add("KYD");
        charCodes.add("KZT");
        charCodes.add("LAK");
        charCodes.add("LBP");
        charCodes.add("LKR");
        charCodes.add("LRD");
        charCodes.add("LSL");
        charCodes.add("LYD");
        charCodes.add("MAD");
        charCodes.add("MDL");
        charCodes.add("MGA");
        charCodes.add("MKD");
        charCodes.add("MMK");
        charCodes.add("MNT");
        charCodes.add("MOP");
        charCodes.add("MRU");
        charCodes.add("MUR");
        charCodes.add("MVR");
        charCodes.add("MWK");
        charCodes.add("MXN");
        charCodes.add("MYR");
        charCodes.add("MZN");
        charCodes.add("NAD");
        charCodes.add("NGN");
        charCodes.add("NIO");
        charCodes.add("NOK");
        charCodes.add("NPR");
        charCodes.add("NZD");
        charCodes.add("OMR");
        charCodes.add("PAB");
        charCodes.add("PEN");
        charCodes.add("PGK");
        charCodes.add("PHP");
        charCodes.add("PKR");
        charCodes.add("PLN");
        charCodes.add("PYG");
        charCodes.add("QAR");
        charCodes.add("RON");
        charCodes.add("RSD");
        charCodes.add("RUB");
        charCodes.add("RWF");
        charCodes.add("SAR");
        charCodes.add("SBD");
        charCodes.add("SCR");
        charCodes.add("SDG");
        charCodes.add("SEK");
        charCodes.add("SGD");
        charCodes.add("SHP");
        charCodes.add("SLL");
        charCodes.add("SOS");
        charCodes.add("SRD");
        charCodes.add("SSP");
        charCodes.add("STD");
        charCodes.add("STN");
        charCodes.add("SVC");
        charCodes.add("SYP");
        charCodes.add("SZL");
        charCodes.add("THB");
        charCodes.add("TJS");
        charCodes.add("TMT");
        charCodes.add("TND");
        charCodes.add("TOP");
        charCodes.add("TRY");
        charCodes.add("TTD");
        charCodes.add("TWD");
        charCodes.add("TZS");
        charCodes.add("UAH");
        charCodes.add("UGX");
        charCodes.add("USD");
        charCodes.add("UYU");
        charCodes.add("UZS");
        charCodes.add("VES");
        charCodes.add("VND");
        charCodes.add("VUV");
        charCodes.add("WST");
        charCodes.add("XAF");
        charCodes.add("XAG");
        charCodes.add("XAU");
        charCodes.add("XCD");
        charCodes.add("XDR");
        charCodes.add("XOF");
        charCodes.add("XPD");
        charCodes.add("XPF");
        charCodes.add("XPT");
        charCodes.add("YER");
        charCodes.add("ZAR");
        charCodes.add("ZMW");
        charCodes.add("ZWL");
        Map<Object, Object> rateClient = Objects.requireNonNull(iRateClient.getRateLatest(openexchangeratesAppId, "RUB").getBody());
        Assertions.assertEquals(ResponseEntity.ok(exchangeRateService.getCharCodes(rateClient)), ResponseEntity.ok(charCodes));
    }
}

