package ru.slovetskikh.exchangerateapp.feign.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import ru.slovetskikh.exchangerateapp.feign.client.IGiphyClient;
import ru.slovetskikh.exchangerateapp.feign.client.IRateClient;
import ru.slovetskikh.exchangerateapp.feign.service.ExchangeRateService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.Objects;

@SpringBootTest
@RunWith(SpringRunner.class)
class FunnyRateControllerTest {

    @Value("${openexchangerates.appId}")
    private String openexchangeratesAppId;

    @Value("${openexchangerates.dateToday}")
    private String dateToday;

    @Value("${openexchangerates.dateYestarday}")
    private String dateYesterday;

    @Autowired
    private IRateClient iRateClient;

    @Autowired
    private IGiphyClient iGiphyClient;

    @Autowired
    private ExchangeRateService exchangeRateService;

    @BeforeAll
    public static void setup() {
        Calendar calendarNow = Calendar.getInstance();
        String formattedData = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateToday", formattedData);

        calendarNow.add(Calendar.DAY_OF_YEAR, -1);
        String formattedData2 = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateYestarday", formattedData2);
    }

    @Test
    void getFunnyRateImg() {
        String openexchangeratesRate = "RUB";
        String today="2021-12-28";
        String yesterday = "2021-12-27";
        double expectedCurrentRateResponse = 73.7487;
        double expectedYesterdayRateResponse = 73.394;

        Map<Object, Object> rateClient = Objects.requireNonNull(iRateClient.getRateHistoryToday(openexchangeratesAppId, today, openexchangeratesRate).getBody());
        double currentRateResponse = exchangeRateService.getRate(rateClient, openexchangeratesRate);
        Map<Object, Object> rateYesterdayClient = Objects.requireNonNull(iRateClient.getRateHistory(openexchangeratesAppId, yesterday, openexchangeratesRate).getBody());
        double yesterdayRateResponse = exchangeRateService.getRate(rateYesterdayClient, openexchangeratesRate);
        String gifId;
        if (expectedCurrentRateResponse - expectedYesterdayRateResponse >= 0) {
            gifId = "YonPNcsnPXpImmSSPt";
        }
        else {
            gifId = "lptjRBxFKCJmFoibP3";
        }
        String gifUrl = exchangeRateService.getGiphyGif(Objects.requireNonNull(iGiphyClient.getMockGiphyRichById(gifId).getBody()));
        String expectedGifUrl = exchangeRateService.getGiphyGif(Objects.requireNonNull(iGiphyClient.getMockGiphyRichById("YonPNcsnPXpImmSSPt").getBody()));
        byte[] gifImage = exchangeRateService.getGifInBytes(gifUrl);
        byte[] expectedGifImage = exchangeRateService.getGifInBytes(expectedGifUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_GIF);
        headers.setContentLength(gifImage.length);

        HttpHeaders headers2 = new HttpHeaders();
        headers2.setContentType(MediaType.IMAGE_GIF);
        headers2.setContentLength(expectedGifImage.length);

        Assertions.assertEquals("YonPNcsnPXpImmSSPt", gifId );
        Assertions.assertEquals(expectedGifUrl, gifUrl);
        Assertions.assertEquals(new HttpEntity<>(expectedGifImage, headers2), new HttpEntity<>(gifImage, headers));
        Assertions.assertEquals(expectedCurrentRateResponse, currentRateResponse);
        Assertions.assertEquals(expectedYesterdayRateResponse, yesterdayRateResponse);
    }
}
