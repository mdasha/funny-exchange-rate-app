package ru.slovetskikh.exchangerateapp.feign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.slovetskikh.exchangerateapp.feign.client.IGiphyClient;
import ru.slovetskikh.exchangerateapp.feign.rest.response.Giphy;
import ru.slovetskikh.exchangerateapp.feign.service.ExchangeRateService;

import java.util.Map;
import java.util.Objects;

@RestController
public class GiphyController {

    private final IGiphyClient iGiphyClient;
    private final ExchangeRateService exchangeRateService;

    @Autowired
    public GiphyController(ExchangeRateService exchangeRateService, IGiphyClient iGiphyClient) {
        this.exchangeRateService = exchangeRateService;
        this.iGiphyClient = iGiphyClient;
    }

    @GetMapping(path = "/giphy/random/rich")
    ResponseEntity<Giphy> getgiphyRich() {
        Map<Object, Object> giphyClient = Objects.requireNonNull(iGiphyClient.getGiphyRichy().getBody());
        Giphy giphy = new Giphy (
                exchangeRateService.getGiphyType(giphyClient),
                exchangeRateService.getGiphyId(giphyClient),
                exchangeRateService.getGiphyUrl(giphyClient),
                exchangeRateService.getGiphyTitle(giphyClient),
                exchangeRateService.getGiphyGif(giphyClient)
        );
        return ResponseEntity.ok(giphy);

    }

    @GetMapping(path = "/giphy/random/broke")
    ResponseEntity<Giphy> getGiphyBroke() {
        Map<Object, Object> giphyClient = Objects.requireNonNull(iGiphyClient.getGiphyBroke().getBody());
        Giphy giphy = new Giphy (
                exchangeRateService.getGiphyType(giphyClient),
                exchangeRateService.getGiphyId(giphyClient),
                exchangeRateService.getGiphyUrl(giphyClient),
                exchangeRateService.getGiphyTitle(giphyClient),
                exchangeRateService.getGiphyGif(giphyClient));
        return ResponseEntity.ok(giphy);
    }
}
