package ru.slovetskikh.exchangerateapp.feign.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import ru.slovetskikh.exchangerateapp.feign.client.IRateClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.slovetskikh.exchangerateapp.feign.rest.response.Exchange;
import ru.slovetskikh.exchangerateapp.feign.service.ExchangeRateService;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
public class RateController {

    @Value("${openexchangerates.appId}")
    private String openexchangeratesAppId;

    @Value("${openexchangerates.dateYestarday}")
    private String dateYesterday;

    @Value("${openexchangerates.dateToday}")
    private String dateToday;

    private final ExchangeRateService exchangeRateService;
    private final IRateClient iRateClient;

    @Autowired
    public RateController(ExchangeRateService exchangeRateService, IRateClient iRateClient) {
        this.exchangeRateService = exchangeRateService;
        this.iRateClient = iRateClient;
    }

    @GetMapping(path = "/rate/history/{openexchangeratesRate}")
    ResponseEntity<Exchange> getRateHistory(@PathVariable String openexchangeratesRate) {
        Map<Object, Object> rateClient = Objects.requireNonNull(iRateClient.getRateHistory(openexchangeratesAppId, dateYesterday, openexchangeratesRate).getBody());
        Exchange exchange = new Exchange (
                exchangeRateService.getDisclaimer(rateClient),
                exchangeRateService.getLicense(rateClient),
                exchangeRateService.getBase(rateClient),
                exchangeRateService.getRate(rateClient, openexchangeratesRate),
                openexchangeratesRate,
                dateYesterday
        );

        return ResponseEntity.ok(exchange);
    }

    @GetMapping(path = "/rate/latest/{openexchangeratesRate}")
    ResponseEntity<Exchange> getRateLatest(@PathVariable String openexchangeratesRate) {
        Map<Object, Object> rateClient = Objects.requireNonNull(iRateClient.getRateLatest(openexchangeratesAppId, openexchangeratesRate).getBody());
        Exchange exchange = new Exchange (
                exchangeRateService.getDisclaimer(rateClient),
                exchangeRateService.getLicense(rateClient),
                exchangeRateService.getBase(rateClient),
                exchangeRateService.getRate(rateClient, openexchangeratesRate),
                openexchangeratesRate,
                dateToday
        );

        return ResponseEntity.ok(exchange);
    }

    @GetMapping(path = "/rate/codes")
    public ResponseEntity<List<String>> getCharCodes() {
        Map<Object, Object> rateClient = Objects.requireNonNull(iRateClient.getRateLatest(openexchangeratesAppId, "RUB").getBody());
        return ResponseEntity.ok(exchangeRateService.getCharCodes(rateClient));
    }
}
