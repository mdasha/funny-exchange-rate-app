package ru.slovetskikh.exchangerateapp.feign.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.slovetskikh.exchangerateapp.feign.client.IGiphyClient;
import ru.slovetskikh.exchangerateapp.feign.client.IRateClient;
import ru.slovetskikh.exchangerateapp.feign.service.ExchangeRateService;

import java.util.Map;
import java.util.Objects;

@RestController
public class FunnyRateController {

    private final ExchangeRateService exchangeRateService;
    private final IRateClient iRateClient;
    private final IGiphyClient iGiphyClient;

    @Value("${openexchangerates.appId}")
    private String openexchangeratesAppId;

    @Value("${openexchangerates.dateYestarday}")
    private String dateYesterday;

    @Autowired
    public FunnyRateController(ExchangeRateService exchangeRateService, IRateClient iRateClient, IGiphyClient iGiphyClient) {
        this.exchangeRateService = exchangeRateService;
        this.iRateClient = iRateClient;
        this.iGiphyClient = iGiphyClient;
    }

    @GetMapping(path = "/rate/funnyRate/{openexchangeratesRate}")
    HttpEntity<byte[]> getFunnyRateImg(@PathVariable String openexchangeratesRate) {
        Map<Object, Object> rateLatest = Objects.requireNonNull(iRateClient.getRateLatest(openexchangeratesAppId, openexchangeratesRate).getBody());
        Map<Object, Object> rateHistory = Objects.requireNonNull(iRateClient.getRateHistory(openexchangeratesAppId, dateYesterday, openexchangeratesRate).getBody());
        double latestRate = exchangeRateService.getRate(rateLatest, openexchangeratesRate);
        double historyRate = exchangeRateService.getRate(rateHistory, openexchangeratesRate);

        String gifUrl;
        byte[] gifImage;
        if (latestRate >= historyRate) {
            gifUrl = exchangeRateService.getGiphyGif(Objects.requireNonNull(iGiphyClient.getGiphyRichy().getBody()));
        }
        else {
            gifUrl = exchangeRateService.getGiphyGif(Objects.requireNonNull(iGiphyClient.getGiphyBroke().getBody()));
        }
        gifImage = exchangeRateService.getGifInBytes(gifUrl);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_GIF);
        headers.setContentLength(gifImage.length);

        return new HttpEntity<>(gifImage, headers);
    }
}
