package ru.slovetskikh.exchangerateapp.feign.service;

import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Service
public class ExchangeRateService {

    public double getRate (Map<Object, Object> responseEntity, String openexchangeratesRate) {
        Map<String, Double> exchangeRates = (Map<String, Double>) responseEntity.get("rates");
        return(exchangeRates.get(openexchangeratesRate));
    }

    public List<String> getCharCodes(Map<Object, Object> responseEntity) {
        Map<String, Double> exchangeRates = (Map<String, Double>) responseEntity.get("rates");
        List<String> charCodes = new LinkedList<>();
        for (Map.Entry<String, Double> entry : exchangeRates.entrySet()) {
            charCodes.add(entry.getKey());
        }
        return charCodes;
    }

    public String getDisclaimer(Map<Object, Object> responseEntity) {
        return (responseEntity.get("disclaimer").toString());
    }

    public String getLicense(Map<Object, Object> responseEntity) {
        return (responseEntity.get("license").toString());
    }

    public String getBase(Map<Object, Object> responseEntity) {
        return (responseEntity.get("base").toString());
    }

    public String getGiphyType(Map<Object, Object> responseEntity) {
        Map<String, String> data = (Map<String, String>) responseEntity.get("data");
        return (data.get("type"));
    }

    public String getGiphyId(Map<Object, Object> responseEntity) {
        Map<String, String> data = (Map<String, String>) responseEntity.get("data");
        return (data.get("id"));
    }

    public String getGiphyUrl(Map<Object, Object> responseEntity) {
        Map<String, String> data = (Map<String, String>) responseEntity.get("data");
        return (data.get("url"));
    }

    public String getGiphyTitle(Map<Object, Object> responseEntity) {
        Map<String, String> data = (Map<String, String>) responseEntity.get("data");
        return (data.get("title"));
    }

    public String getGiphyMp4(Map<Object, Object> responseEntity) {
        Map<String, Map<String,  Map<String, String>>> data = ( Map<String, Map<String,  Map<String, String>>>) responseEntity.get("data");
        Map<String,  Map<String, String>> images = data.get("images");
        Map<String, String> original = images.get("original");
        return (original.get("mp4"));
    }

    public String getGiphyGif(Map<Object, Object> responseEntity) {
        Map<String, String> data = (Map<String, String>) responseEntity.get("data");
        return ("https://i.giphy.com/" + data.get("id") + ".gif");
    }

    public byte[] getGifInBytes(String gifUrl) {
        try (InputStream inputStream = new BufferedInputStream((new URL(gifUrl)).openStream())) {
            return IOUtils.toByteArray(inputStream);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return null;
    }
}
