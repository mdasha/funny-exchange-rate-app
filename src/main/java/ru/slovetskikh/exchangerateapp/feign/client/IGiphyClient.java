package ru.slovetskikh.exchangerateapp.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "giphy", url = "${giphy.uri}")
public interface IGiphyClient {

    @RequestMapping(method = RequestMethod.GET, value = "/random?api_key=${giphy.appKey}&tag=${giphy.tag1}&rating=${giphy.rating}")
    ResponseEntity<Map<Object, Object>> getGiphyRichy();

    @RequestMapping(method = RequestMethod.GET, value = "/random?api_key=${giphy.appKey}&tag=${giphy.tag2}&rating=${giphy.rating}")
    ResponseEntity<Map<Object, Object>> getGiphyBroke();

    @RequestMapping(method = RequestMethod.GET, value = "/{giphyGifId}?api_key=${giphy.appKey}")
    ResponseEntity<Map<Object, Object>> getMockGiphyRichById(@PathVariable String giphyGifId);
}
