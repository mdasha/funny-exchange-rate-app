package ru.slovetskikh.exchangerateapp.feign.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "exchangeRate", url = "${openexchangerates.uri}")
public interface IRateClient {

    @RequestMapping(method = RequestMethod.GET, value = "/historical/{dateYestarday}.json?app_id={appId}")
    ResponseEntity<Map<Object, Object>> getRateHistory(@RequestParam("app_id") String appId, @RequestParam("dateYestarday") String dateToday, String exchangeRate);

    @RequestMapping(method = RequestMethod.GET, value = "/latest.json?app_id={appId}")
    ResponseEntity<Map<Object, Object>> getRateLatest(@RequestParam("app_id") String appId, String exchangeRate);

    @RequestMapping(value = "/historical/{dateToday}.json?app_id={appId}", method = RequestMethod.GET)
    ResponseEntity<Map<Object, Object>> getRateHistoryToday(@RequestParam("app_id") String appId, @RequestParam("dateToday") String dateToday, String exchangeRate);

}
