package ru.slovetskikh.exchangerateapp.feign.rest.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Exchange {
    private final String disclaimer;
    private final String license;
    private final String base;
    private final Double rate;
    private final String currency;
    private final String date;
}
