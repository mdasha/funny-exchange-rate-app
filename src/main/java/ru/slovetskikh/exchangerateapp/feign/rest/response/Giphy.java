package ru.slovetskikh.exchangerateapp.feign.rest.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class Giphy {
    private final String type;
    private final String id;
    private final String url;
    private final String title;
    private final String gif;
}
