package ru.slovetskikh.exchangerateapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@SpringBootApplication
@EnableFeignClients(basePackages = {"ru.slovetskikh.exchangerateapp.feign",
        "ru.slovetskikh.exchangerateapp.feign.controller"})
public class ExchangeRateAppApplication {

    public static void main(String[] args) {

        Calendar calendarNow = Calendar.getInstance();
        String formattedData = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateToday", formattedData);

        calendarNow.add(Calendar.DAY_OF_YEAR, -1);
        String formattedData2 = new SimpleDateFormat("yyyy-MM-dd").format(calendarNow.getTime());
        System.setProperty("openexchangerates.dateYestarday", formattedData2);

        SpringApplication.run(ExchangeRateAppApplication.class, args);
    }
}
